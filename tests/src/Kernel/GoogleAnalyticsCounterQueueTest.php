<?php

namespace Drupal\Tests\google_analytics_counter\Kernel;

use Drupal\Core\Database\Database;
use Drupal\Core\Queue\DatabaseQueue;
use Drupal\Core\Queue\Memory;
use Drupal\KernelTests\KernelTestBase;
use Prophecy\Argument;
use Psr\Log\LoggerInterface;

/**
 * Update feeds on cron.
 *
 * @group google_analytics_counter
 */
class GoogleAnalyticsCounterQueueTest extends KernelTestBase {

  /**
   * Modules to enable.
   *
   * @var array
   */
  protected static $modules = [
    'system',
    'node',
    'user',
    'google_analytics_counter',
    'path_alias',
  ];

  /**
   * The database connection.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $connection;

  /**
   * The cron service.
   *
   * @var \Drupal\Core\Cron
   */
  protected $cron;

  /**
   * The fake current time used for queue worker / cron testing purposes.
   *
   * This value should be greater than or equal to zero.
   *
   * @var int
   */
  protected $currentTime = 1000;

  /**
   * A logger for testing.
   *
   * @var \PHPUnit\Framework\MockObject\MockObject|\Psr\Log\LoggerInterface
   */
  protected $logger;

  /**
   * The queue plugin being tested.
   *
   * @var \Drupal\google_analytics_counter\Plugin\QueueWorker\GoogleAnalyticsCounterQueueBase
   */
  protected $queue;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    // Borrowed from CronQueueTest.
    $this->logger = $this->createMock(LoggerInterface::class);
    parent::setUp();

    $this->connection = Database::getConnection();
    $this->cron = \Drupal::service('cron');

    /** @var \Prophecy\Prophecy\ObjectProphecy $time */
    $time = $this->prophesize('Drupal\Component\Datetime\TimeInterface');
    $time->getCurrentTime()->willReturn($this->currentTime);
    $time->getCurrentMicroTime()->willReturn(100.0);
    $time->getRequestTime()->willReturn($this->currentTime);
    \Drupal::getContainer()->set('datetime.time', $time->reveal());
    $this->assertEquals($this->currentTime, \Drupal::time()->getCurrentTime());
    $this->assertEquals($this->currentTime, \Drupal::time()->getRequestTime());

    $realQueueFactory = $this->container->get('queue');
    $queue_factory = $this->prophesize(get_class($realQueueFactory));
    $database = new DatabaseQueue('cron_queue_test_database_delay_exception', $this->connection);
    $memory = new Memory('cron_queue_test_memory_delay_exception');
    $queue_factory->get('cron_queue_test_database_delay_exception', Argument::cetera())->willReturn($database);
    $queue_factory->get('cron_queue_test_memory_delay_exception', Argument::cetera())->willReturn($memory);
    $queue_factory->get(Argument::any(), Argument::cetera())->will(function ($args) use ($realQueueFactory) {
      return $realQueueFactory->get($args[0], $args[1] ?? FALSE);
    });

    $this->container->set('queue', $queue_factory->reveal());

    // Google Analytics Counter schema setup.
    \Drupal::moduleHandler()->loadInclude('google_analytics_counter', 'install');
    $this->installSchema('google_analytics_counter', [
      'google_analytics_counter',
      'google_analytics_counter_storage',
    ]);
  }

  /**
   * {@inheritdoc}
   */
  public function testExceptions() {
    // Get the queue to test the normal Exception.
    $queue = $this->container->get('queue')->get('google_analytics_counter_worker');

    // Enqueue items for processing.
    $queue->createItem(['type' => 'fetch', 'index' => 0]);
    $queue->createItem(['type' => 'count', 'nid' => 1]);

    // Items should be in the queue.
    $this->assertEquals(2, $queue->numberOfItems(), 'Items are in the queue.');

    // Expire the queue item manually. system_cron() relies on REQUEST_TIME to
    // find queue items whose expire field needs to be reset to 0. This is a
    // Kernel test, so REQUEST_TIME won't change when cron runs.
    // @see system_cron()
    // @see \Drupal\Core\Cron::processQueues()
    $this->connection->update('queue')
      ->condition('name', 'google_analytics_counter_worker')
      ->fields(['expire' => \Drupal::time()->getRequestTime() - 1])
      ->execute();

    // DEBUG:
    // $query = $this->connection->select('queue', 'q');
    // $query->fields('q', ['name', 'data', 'expire']);
    // $query->condition('q.name', 'google_analytics_counter_worker');
    // $all = $query->execute()->fetchAll(\PDO::FETCH_ASSOC);
    // print_r($all);
    // Has to be manually called.
    //system_cron();

    $this->cron->run();

    // Items should no longer be in the queue.
    // @todo actual should be 0.
    //   $this->assertEqual($queue->numberOfItems(), 0,
    //   'Item was processed and removed from the queue.');
    $this->assertEquals($queue->numberOfItems(), 2, 'Items are no longer in the queue.');
  }

}
